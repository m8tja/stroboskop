# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://m8tja@bitbucket.org/m8tja/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/m8tja/stroboskop/commits/7b763fec49249b19f06ff1ccb07c4fc76bce613a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/m8tja/stroboskop/commits/027fcb3152348f98db08436e2311bcef9e8fa5cd

Naloga 6.3.2:
https://bitbucket.org/m8tja/stroboskop/commits/eada361f6c7204c736f876d1ff177ea13afb6b7b

Naloga 6.3.3:
https://bitbucket.org/m8tja/stroboskop/commits/6766a08355b71db7fc9474e622484b1f8cc8b2ea

Naloga 6.3.4:
https://bitbucket.org/m8tja/stroboskop/commits/4ea6742626029aee35fe9bb7d9abb177e000a6e4

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/m8tja/stroboskop/commits/477b77b565e296cb658281a25c89cc1a35a6b8ba

Naloga 6.4.2:
https://bitbucket.org/m8tja/stroboskop/commits/d6dcbe959289ca78adcfae265dd3fac96a7a2604

Naloga 6.4.3:
https://bitbucket.org/m8tja/stroboskop/commits/f2e20d64fec5fbe5e08cd44798b526c32ea133e2

Naloga 6.4.4:
https://bitbucket.org/m8tja/stroboskop/commits/97a3bd036421955fef1aaff6d6b5e60a3d769358